import { Component, OnInit } from '@angular/core';
import {ElementoConfiguracionService} from '../api/elemento-configuracion.service';
import {ElementoConfiguracion} from '../models/elemento-configuracion';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-elementos-configuracion',
  templateUrl: './elementos-configuracion.component.html',
  styleUrls: ['./elementos-configuracion.component.css']
})
export class ElementosConfiguracionComponent implements OnInit {

  configurationElements: ElementoConfiguracion[];

  constructor(
    private CIService: ElementoConfiguracionService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getElementos();
  }

  getElementos(){
    this.CIService.index().subscribe(res => {
      this.configurationElements = res;
    });
  }

  editCI(element: ElementoConfiguracion){
    this.router.navigateByUrl(`admin/recursos/${element.idElementoConf}`);
  }

  deleteCI(element: ElementoConfiguracion){

    Swal.fire({
      title: '¿Seguro que deseas eliminar el elemento de configuración?',
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.CIService.delete(element.idElementoConf).subscribe(res => {
          Swal.fire(
            'Elemento de configuración eliminado',
            'El elemento de configuración ha sido elimnado correctamente',
            'success'
          );
          this.getElementos();
        });
      }
    });

  }
}
