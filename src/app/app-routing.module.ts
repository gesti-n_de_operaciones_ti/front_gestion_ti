import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './network/auth.guard';
import { LoginComponent } from './pages/login/login.component';
import { LoginGuard } from './network/login.guard';
import { AdminGuard } from './network/admin.guard';
import { NotFoundComponent } from './shared-module/not-found/not-found.component';
import {TecnicoGuard} from './network/tecnico.guard';
import {ProfesoresGuard} from './network/profesores.guard';


const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path : 'login' ,
    component : LoginComponent,
    canActivate : [LoginGuard]
  },
  {
    path: 'admin',
    canActivate: [AuthGuard, AdminGuard],
    loadChildren: () => import('./admin/admin.module').
    then(m => m.AdminModule)
  },
  {
    path: 'tecnico',
    canActivate: [AuthGuard, TecnicoGuard],
    loadChildren: () => import('./empleados/empleados.module').
    then(m => m.EmpleadosModule)
  },
  {
    path: 'profesor',
    canActivate: [AuthGuard, ProfesoresGuard],
    loadChildren: () => import('./empleados/empleados.module').
    then(m => m.EmpleadosModule)
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 }

