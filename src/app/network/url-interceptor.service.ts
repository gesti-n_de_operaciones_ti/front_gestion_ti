import { Injectable, Output, EventEmitter} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {environment} from '../../environments/environment';
import {catchError} from "rxjs/internal/operators";
import { ErrorHandlerService } from '../api/error-handler.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UrlInterceptorService implements HttpInterceptor{

  @Output() error: EventEmitter<any> = new EventEmitter(true);

  constructor( 
    private errorHandler: ErrorHandlerService,
    private router: Router) {
    errorHandler.errorEvent.subscribe(res =>{      
      this.error.emit(res); 
    })    
   }
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(req.url.includes('https://') 
      || req.url.includes('http://')
      || req.url.includes('assets/i18n/')
    ){
      return next.handle(req.clone());
    }

    const url = `http://${window.location.hostname}:8000/api/`;
    //environment.API
    return next.handle(req.clone({url : `${url}${req.url}`})).pipe(
      catchError(this.errorHandler.handleError())
    );
  }
  
  
}
