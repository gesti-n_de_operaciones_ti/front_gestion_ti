import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {StorageService} from '../api/storage.service';

@Injectable({
  providedIn: 'root'
})
export class ProfesoresGuard implements CanActivate {

  constructor(private storage: StorageService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const user = this.storage.getEmployee();

    switch (user.role){
      case 1:
        this.router.navigateByUrl('/admin');
        break;
      case 2:
        this.router.navigateByUrl('/tecnico');
        break;
    }

    return user.role === 3;
  }

}
