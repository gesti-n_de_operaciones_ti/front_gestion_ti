import { Component } from '@angular/core';
import { ErrorHandlerService } from './api/error-handler.service';
import { UrlInterceptorService } from './network/url-interceptor.service';
import Swal from 'sweetalert2';
import { setTheme } from 'ngx-bootstrap/utils';
import {BsLocaleService} from 'ngx-bootstrap/datepicker';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Gestion de Ti';

  constructor(
    urlInterceptor: UrlInterceptorService,
    private bsLocaleService: BsLocaleService
  ) {
    urlInterceptor.error.subscribe(res => {
      Swal.fire({
        icon: 'warning',
        title: 'Advertencia',
        text: res.msg
      });
      console.log(res);
    });

    }
}
