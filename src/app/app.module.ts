import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { SharedModule } from './shared-module/shared-module.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UrlInterceptorService } from './network/url-interceptor.service';
import { JwtModule, JWT_OPTIONS } from '@auth0/angular-jwt';
import { StorageService } from './api/storage.service';
import { ModalCreateEmployeeComponent } from './components/modal-create-employee/modal-create-employee.component';
import {NgHttpLoaderModule} from 'ng-http-loader';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DatePipe} from '@angular/common';
import { ModalElementoConfiguracionComponent } from './components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {DetalleSolicitudComponent} from './shared-module/detalle-solicitud/detalle-solicitud.component';
import {FeedbackValidatorsComponent} from './components/feedback-validator/feedback-validators.component';


export function tokenGetter(storageService: StorageService){
  return {
    tokenGetter: () => {
      return storageService.getToken();
    }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ModalCreateEmployeeComponent,
    ModalElementoConfiguracionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NgHttpLoaderModule.forRoot(),
    JwtModule.forRoot({
      config: {
        allowedDomains: ['localhost:8000'],
        throwNoTokenError: true,
        skipWhenExpired: true
      },
      jwtOptionsProvider: {
        provide: JWT_OPTIONS,
        useFactory: tokenGetter,
        deps: [StorageService]
      }
    }),

  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: UrlInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
