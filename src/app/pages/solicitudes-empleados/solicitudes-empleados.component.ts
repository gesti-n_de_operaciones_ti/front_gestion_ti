import { Component, OnInit } from '@angular/core';
import {Employee} from '../../models/employee';
import {StorageService} from '../../api/storage.service';
import {Solicitud} from '../../models/solicitud';
import {SolicitudesService} from '../../api/solicitudes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-solicitudes-empleados',
  templateUrl: './solicitudes-empleados.component.html',
  styleUrls: ['./solicitudes-empleados.component.css']
})
export class SolicitudesEmpleadosComponent implements OnInit {

  solicitudes: Solicitud[];

  solicitudSelected: Solicitud;
  bsModalRef: BsModalRef;
  action = 'table';

  constructor(
    private solicitudesService: SolicitudesService,
    private router: Router,
    private route: ActivatedRoute,
    private storage: StorageService,
    private modalService: BsModalService
  ) { }

  ngOnInit(): void {

    const params = {
      1: {},
      2: {idTecnico: this.user.id},
      3: {idSolicitante: this.user.id}
    };

    this.solicitudesService.index(params[this.user.role]).subscribe(res =>{
      this.solicitudes = res;
    });
  }

  get user(): Employee{
    return this.storage.getEmployee();
  }

  openElementoModal(idElementoConf: string){
    const initialState = {
      idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

  verDetalleSolicitud(solicitud: Solicitud){
    this.router.navigate([solicitud.idSolicitud, 'historial'],
      {relativeTo: this.route});
  }

  asignarIncidencia(solicitud: Solicitud){
    this.router.navigateByUrl(`/admin/incidencias/${solicitud.idSolicitud}/asignar`);
  }

  diagnosticarIncidencia(solicitud: Solicitud){
    this.router.navigateByUrl(`/tecnico/incidencias/${solicitud.idSolicitud}/diagnosticar`);
  }

  procesarIncidencia(solicitud: Solicitud) {
    this.router.navigateByUrl(`/tecnico/incidencias/${solicitud.idSolicitud}/procesar`);
  }

  evaluarIncidencia(solicitud: Solicitud) {
    this.router.navigateByUrl(`/profesor/incidencias/${solicitud.idSolicitud}/evaluar`);
  }
}
