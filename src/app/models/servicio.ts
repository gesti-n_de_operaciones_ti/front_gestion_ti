export interface Servicio{
  idProblema?: string;
  nbServicio?: string;
  descripcion?: string;
  solucion?: string;
  tiempoEstimado?: number;
}
