export interface Edificio{
  idEdificio?: string;
  clave?: string;
  descripcion?: string;
  descUbicacion?: string;
}

export interface Localizacion{
  idLocalizacion?: number;
  claveLocalizacion?: string;
  descUbicacion?: string;
  idEdificio?: string;
  descEdificio?: string;
  claveEdificio?: string;
  numPlanta?: number;
}
