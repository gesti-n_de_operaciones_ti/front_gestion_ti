export interface SolicitudCambio{
  idEstatusCambio?: string;
  nbEstatusCambio?: string;
  idSolicitudCambio?: string;
  idIncidencia?: string;
  idElementoConf?: string;
  descripcion?: string;
  created_at?: string;

  nbElemento?: string;
  descripcionCI?: string;

  nbEncargado?: string;
}
