import {Department} from './departamentos';

export interface Employee{
    email?: string;
    id?: number;
    last_name?: string;
    username?: string;
    name?: string;
    role?: number;
    phone?: string;
    address?: string;
    departamento?: Department[];
}

export interface Role{
  idRol?: number;
  nbRol?: string;
}

