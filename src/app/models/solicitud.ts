import {ElementoConfiguracion} from './elemento-configuracion';
import {Employee} from './employee';
import {QuestionControlService} from '../modules/dforms/question-control-service';

export interface Solicitud {
  idSolicitud?: string;
  descripcion?: string;
  descripcionServicio?: string;
  descripcionSolucion?: string;
  fhInicio?: string;
  fhTerminacion?: string;
  tiempoEstimado?: string;

  idPrioridad?: string;
  nbPrioridad?: string;

  idTecnico?: string;
  nbTecnico?: string;
  apellidoTecnico?: string;

  idSolicitante?: string;
  nbSolicitante?: string;
  apellidoSolicitante?: string;

  idEstatus?: string;
  nbEstatus?: string;

  idElementoConf?: string;
  nbElemento?: string;

  elementoConfiguracion?: ElementoConfiguracion;
  solicitante?: Employee;
  tecnico?: Employee;
}

export interface HistorialSolicitud{
  idHistorial?: number;
  idEstatus?: number;
  nbEstatus?: string;
  idSolicitud?: number;
  fhCambio?: string;
}

export interface Prioridad {
  idPrioridad?: string;
  nbPrioridad?: string;
}

export interface EvaluacionIncidencia {
  idEvaluacion?: number;
  resena?: string;
  calificacion?: number;
  questionario_data?: any;
  quiz?: any;
  created_at?: string;
}
