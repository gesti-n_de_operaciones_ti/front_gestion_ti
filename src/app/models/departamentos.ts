import {EmpleadosComponent} from '../admin/pages/empleados/empleados.component';
import {Employee} from './employee';

export interface Department{
  idDepartamento?: number;
  nbDepartamento?: string;
  encargado?: Employee;
}
