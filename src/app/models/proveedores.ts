
export interface Proveedor{
  idProveedor?: number;
  nbProveedor?: number;
  rfc?: string;
  email?: string;
  telefono?: string;
  direccion?: string;
}
