import {Validators} from '@angular/forms';

export interface TipoCI{
  siglasTipoCI?: number;
  nbTipoCI?: string;
}

export interface ElementoConfiguracion{
  idElementoConf?: string;
  descripcion?: string;
  nbElemento?: string;
  idEncargado?: number;
  idProveedor?: number;
  siglasTipoCI?: string;
  nbTipoCI?: string;
  nbEncargado?: string;
  nbProveedor?: string;
  fhAdquisicion?: string;
  data_ci?: any;
}
