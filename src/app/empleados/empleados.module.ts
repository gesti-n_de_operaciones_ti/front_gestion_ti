import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmpleadosRoutingModule } from './empleados-routing.module';
import { EmpleadosPanelComponent } from './empleados-panel/empleados-panel.component';
import {SharedModule} from '../shared-module/shared-module.module';
import { FormSolicitudesCreateComponent } from './form-solicitudes-create/form-solicitudes-create.component';
import { CatalogoServiciosComponent } from './catalogo-servicios/catalogo-servicios.component';
import { FormCatalogoServiciosComponent } from '../shared-module/form-catalogo-servicios/form-catalogo-servicios.component';
import { FormDiagnosticoSolicitudComponent } from './form-diagnostico-solicitud/form-diagnostico-solicitud.component';
import { FormProcesarSolicitudComponent } from './form-procesar-solicitud/form-procesar-solicitud.component';
import { FormEvaluarSolicitudComponent } from './form-evaluar-solicitud/form-evaluar-solicitud.component';

@NgModule({
  declarations: [
    EmpleadosPanelComponent,
    FormSolicitudesCreateComponent,
    CatalogoServiciosComponent,
    FormCatalogoServiciosComponent,
    FormDiagnosticoSolicitudComponent,
    FormProcesarSolicitudComponent,
    FormEvaluarSolicitudComponent
  ],
  exports: [
  ],
  imports: [
    CommonModule,
    EmpleadosRoutingModule,
    SharedModule
  ]
})
export class EmpleadosModule { }
