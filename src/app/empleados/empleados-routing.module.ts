import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmpleadosPanelComponent} from './empleados-panel/empleados-panel.component';
import {SolicitudesEmpleadosComponent} from '../pages/solicitudes-empleados/solicitudes-empleados.component';
import {FormSolicitudesCreateComponent} from './form-solicitudes-create/form-solicitudes-create.component';
import {EmpleadosComponent} from '../admin/pages/empleados/empleados.component';
import {CatalogoServiciosComponent} from './catalogo-servicios/catalogo-servicios.component';
import {FormCatalogoServiciosComponent} from '../shared-module/form-catalogo-servicios/form-catalogo-servicios.component';
import {ProfesoresGuard} from '../network/profesores.guard';
import {FormAsignarSolicitudComponent} from '../admin/components/form-asignar-solicitud/form-asignar-solicitud.component';
import {FormDiagnosticoSolicitudComponent} from './form-diagnostico-solicitud/form-diagnostico-solicitud.component';
import {TecnicoGuard} from '../network/tecnico.guard';
import {FormProcesarSolicitudComponent} from './form-procesar-solicitud/form-procesar-solicitud.component';
import {FormEvaluarSolicitudComponent} from './form-evaluar-solicitud/form-evaluar-solicitud.component';
import {CambiosSolicitudesComponent} from '../shared-module/cambios-solicitudes/cambios-solicitudes.component';
import {DetalleSolicitudComponent} from '../shared-module/detalle-solicitud/detalle-solicitud.component';


const routes: Routes = [
  {path: '', redirectTo: 'incidencias', pathMatch: 'full'},
  {
    path: '',
    component : EmpleadosPanelComponent,
    children: [
      {
        path: 'incidencias',
        children: [
          {
            path: 'create',
            component: FormSolicitudesCreateComponent,
            canActivate: [ProfesoresGuard]
          },
          {
            path: ':idIncidencia',
            children: [
              {
                path: 'diagnosticar',
                component: FormDiagnosticoSolicitudComponent,
                canActivate: [TecnicoGuard]
              },
              {
                path: 'procesar',
                component: FormProcesarSolicitudComponent,
                canActivate: [TecnicoGuard]
              },
              {
                path: 'evaluar',
                component: FormEvaluarSolicitudComponent,
                canActivate: [ProfesoresGuard]
              },
              {
                path: 'historial',
                component: DetalleSolicitudComponent
              }
            ]
          },
          { path: '', component : SolicitudesEmpleadosComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'catalogo-servicios',
        children: [
          {
            path: 'create',
            component: FormCatalogoServiciosComponent
          },
          { path: '', component : CatalogoServiciosComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'cambios',
        children: [
          {path: '', component: CambiosSolicitudesComponent, pathMatch: 'full'}
        ]
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmpleadosRoutingModule { }
