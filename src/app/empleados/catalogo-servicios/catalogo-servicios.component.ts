import { Component, OnInit } from '@angular/core';
import {CatalogoServiciosService} from '../../api/catalogo-servicios.service';
import {Servicio} from '../../models/servicio';

@Component({
  selector: 'app-catalogo-servicios',
  templateUrl: './catalogo-servicios.component.html',
  styleUrls: ['./catalogo-servicios.component.css']
})
export class CatalogoServiciosComponent implements OnInit {

  catalogoServicios: Servicio[];

  constructor(
    private catalogoService: CatalogoServiciosService
  ) { }

  ngOnInit(): void {
    this.catalogoService.index().subscribe( res => {
      this.catalogoServicios = res;
    });
  }

}
