import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Solicitud} from '../../models/solicitud';
import {SolicitudesService} from '../../api/solicitudes.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {QuestionBase} from '../../modules/dforms/question-base';

@Component({
  selector: 'app-form-evaluar-solicitud',
  templateUrl: './form-evaluar-solicitud.component.html',
  styleUrls: ['./form-evaluar-solicitud.component.css']
})
export class FormEvaluarSolicitudComponent implements OnInit {

  form: FormGroup;
  formEvaluation: FormGroup;

  bsModalRef: BsModalRef;
  submitted = false;
  idSolicitud: string;

  solicitud: Solicitud;
  quiz: any;

  constructor(
    private fb: FormBuilder,
    private solicitudesService: SolicitudesService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.idSolicitud = this.route.snapshot.params.idIncidencia;

    this.form = this.fb.group({
      resena: [null, [Validators.required]],
      calificacion: [null, [Validators.required, Validators.min(1), Validators.max(10)]],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.solicitudesService.quizEvaluacion().subscribe(res => {
      this.quiz = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;
    if (this.form.invalid || this.formEvaluation.invalid) { return; }

    const dataForm = this.form.value;
    dataForm.quizData = JSON.stringify(this.formEvaluation.value);

    this.solicitudesService.evaluar(this.idSolicitud, dataForm).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Incidencia aprobada y guardada.',
        text: 'La incidencia ha sido aprobada.'
      });

      this.router.navigateByUrl('tecnico');
    });

  }

  formQuizCreated(formQuiz: FormGroup){
    this.formEvaluation = formQuiz;
  }
}
