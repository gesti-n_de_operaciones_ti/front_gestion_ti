import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Employee} from '../../models/employee';
import {Prioridad, Solicitud} from '../../models/solicitud';
import {EmployeesService} from '../../api/employees.service';
import {SolicitudesService} from '../../api/solicitudes.service';
import {ActivatedRoute, Router} from '@angular/router';
import Swal from 'sweetalert2';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';

@Component({
  selector: 'app-form-procesar-solicitud',
  templateUrl: './form-procesar-solicitud.component.html',
  styleUrls: ['./form-procesar-solicitud.component.css']
})
export class FormProcesarSolicitudComponent implements OnInit {

  form: FormGroup;
  bsModalRef: BsModalRef;
  submitted = false;
  idSolicitud: string;

  solicitud: Solicitud;

  constructor(
    private fb: FormBuilder,
    private solicitudesService: SolicitudesService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.idSolicitud = this.route.snapshot.params.idIncidencia;

    this.form = this.fb.group({
      descripcionSolucion: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.solicitudesService.show(this.idSolicitud).subscribe(res => {
      this.solicitud = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;
    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.solicitudesService.procesar(this.idSolicitud, dataForm).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Incidencia procesada exitosamente.',
        text: 'La incidencia está a la espera de ser evaluada por el empleado que la solicitó.'
      });

      this.router.navigateByUrl('admin');
    });

  }

  openElementoModal(elemento: ElementoConfiguracion){
    const initialState = {
      idElementoConf: elemento.idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }
}
