import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Prioridad} from '../../models/solicitud';
import {Edificio, Localizacion} from '../../models/EdificiosLocalizaciones';
import {EdificiosLocalizacionesService} from '../../api/edificios-localizaciones.service';
import {ElementoConfiguracion, TipoCI} from '../../models/elemento-configuracion';
import {ElementoConfiguracionService} from '../../api/elemento-configuracion.service';
import {SolicitudesService} from '../../api/solicitudes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-solicitudes-create',
  templateUrl: './form-solicitudes-create.component.html',
  styleUrls: ['./form-solicitudes-create.component.css']
})
export class FormSolicitudesCreateComponent implements OnInit {

  form: FormGroup;

  edificios: Edificio[];
  tiposCI: TipoCI[];
  localizaciones: Localizacion[];
  ciElements: ElementoConfiguracion[];

  siglasTipoCI: string;
  submitted = false;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private localizacionService: EdificiosLocalizacionesService,
    private CIService: ElementoConfiguracionService,
    private solicitudesService: SolicitudesService
  ) {
    this.form = this.fb.group({
      descripcion: [null, [Validators.required]],
      idElementoConf: [null, [Validators.required]],
      siglasTipoCI: [null, [Validators.required]],
      idEdificio: [null],
      idLocalizacion: [null],
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  get formControls(){
    return this.form.controls;
  }

  getData(){
    this.localizacionService.edificios().subscribe(res => {
      this.edificios = res;
    });

    this.CIService.tipos().subscribe(res => {
      this.tiposCI = res;
    });
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.solicitudesService.create(dataForm).subscribe(res => {
      console.log(res);

      Swal.fire({
        icon: 'success',
        title: 'Ha levantado la incidencia.',
        text: 'Un administrador del sistema la procesará en brevedad.'
      });

      this.router.navigateByUrl('/profesor/incidencias');
    });
  }

  changeEdificio(idEdificio: string){
    this.localizacionService.index(null, idEdificio).subscribe(res => {
      this.localizaciones = res;
      this.form.get('idLocalizacion').reset(null);
      this.form.get('idElementoConf').reset(null);
    });
  }

  changeLocalizacion(idLocalizacion: string){
    this.CIService.index({idLocalizacion}).subscribe(res => {
      this.ciElements = res;
    });
  }

  changeTipoCI(tipoCI: string, dataCI = null){
    this.siglasTipoCI = tipoCI;
    this.ciElements = [];

    this.form.get('idElementoConf').setValue(null);

    if (this.siglasTipoCI !== 'HW'){
      this.CIService.index({siglasTipoCI: this.siglasTipoCI}).subscribe(res => {
        this.ciElements = res;
      });
    }
  }

}

