import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {Employee} from '../../models/employee';
import {Prioridad, Solicitud} from '../../models/solicitud';
import {EmployeesService} from '../../api/employees.service';
import {SolicitudesService} from '../../api/solicitudes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {ModalCatalogoServiciosComponent} from '../../components/modal-catalogo-servicios/modal-catalogo-servicios.component';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-diagnostico-solicitud',
  templateUrl: './form-diagnostico-solicitud.component.html',
  styleUrls: ['./form-diagnostico-solicitud.component.css']
})
export class FormDiagnosticoSolicitudComponent implements OnInit {

  form: FormGroup;
  bsModalRef: BsModalRef;
  idSolicitud: string;

  submitted = false;
  checkSolicitudCambio = false;

  solicitud: Solicitud;

  constructor(
    private fb: FormBuilder,
    private empleadosService: EmployeesService,
    private solicitudesService: SolicitudesService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.idSolicitud = this.route.snapshot.params.idIncidencia;

    this.form = this.fb.group({
      descripcionServicio: [null, [Validators.required]],
      tiempoEstimado: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.solicitudesService.show(this.idSolicitud).subscribe(res => {
      this.solicitud = res;
    });
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.solicitudesService.diagnosticar(this.idSolicitud, dataForm).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Incidencia diagnosticada exitosamente.',
        text: 'Ha diagnosticado la incidencia correctamente, puede darla por terminada o esperar a los cambios correspondientes.'
      });

      this.router.navigateByUrl('tecnico');
    });

  }

  get formControls() {
    return this.form.controls;
  }

  openModalCatalogoServicios(){
    const initialState = {};

    this.bsModalRef = this.modalService.show(ModalCatalogoServiciosComponent,
      {initialState , class: 'modal-xl'});
    this.bsModalRef.content.close = () => {

    };
  }

  openElementoModal(elemento: ElementoConfiguracion){
    const initialState = {
      idElementoConf: elemento.idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

  changeCheckCambio(event: any){
    this.checkSolicitudCambio = !this.checkSolicitudCambio;

    if (this.checkSolicitudCambio){
      this.form.addControl('descripcionCambio', new FormControl(null, Validators.required));
    }else{
      this.form.removeControl('descripcionCambio');
    }
  }
}
