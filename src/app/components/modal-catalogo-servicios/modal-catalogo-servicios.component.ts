import { Component, OnInit } from '@angular/core';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {ElementoConfiguracionService} from '../../api/elemento-configuracion.service';
import {CatalogoServiciosService} from '../../api/catalogo-servicios.service';
import {Servicio} from '../../models/servicio';

@Component({
  selector: 'app-modal-catalogo-servicios',
  templateUrl: './modal-catalogo-servicios.component.html',
  styleUrls: ['./modal-catalogo-servicios.component.css']
})
export class ModalCatalogoServiciosComponent implements OnInit {

  catalogoServicios: Servicio[];
  close: any;

  constructor(
    public bsModalRef: BsModalRef,
    private catalogoService: CatalogoServiciosService,
  ) { }

  ngOnInit(): void {
    this.catalogoService.index().subscribe(res => {
      this.catalogoServicios = res;
    });
  }


}
