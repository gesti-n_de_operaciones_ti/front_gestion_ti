import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmployeesService} from '../../api/employees.service';
import {DepartmentsService} from '../../api/departments.service';
import {EdificiosLocalizacionesService} from '../../api/edificios-localizaciones.service';
import {Edificio} from '../../models/EdificiosLocalizaciones';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-localizaciones',
  templateUrl: './form-localizaciones.component.html',
  styleUrls: ['./form-localizaciones.component.css']
})
export class FormLocalizacionesComponent implements OnInit {

  form: FormGroup;
  edificios: Edificio[];

  idLocalizacion: string;
  submitted = false;
  action: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private localizacionesService: EdificiosLocalizacionesService
  ) {
    this.form = this.fb.group({
      claveLocalizacion: [null, [Validators.minLength(2), Validators.maxLength(10)]],
      descUbicacion: [null],
      idEdificio: [null, [Validators.required]],
      numPlanta: [null, [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.idLocalizacion = this.route.snapshot.params.idLocalizacion;

    this.route.data.subscribe(data => {
      this.action = data.action;
    });

    if (this.action === 'show'){
      this.localizacionesService.show(this.idLocalizacion).subscribe(data => {
        this.form.reset(data);
      });
    }

    this.getData();
  }

  get formControls(){
    return this.form.controls;
  }

  getData(){
    this.localizacionesService.edificios().subscribe(res => {
      this.edificios = res;
    });
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    if (this.action === 'create'){
      this.localizacionesService.create(dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Localización creada exitosamente.',
          text: 'Ha registrado el aula o cubículo con éxito.'
        });

        this.router.navigateByUrl('/admin/localizaciones');
      });
    }
    else{
      this.localizacionesService.update(this.idLocalizacion, dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Localización actualizada correctamente.',
          text: 'Los datos del empleado se han actualizado.'
        });

        this.router.navigateByUrl('/admin/localizaciones');
      });

    }
  }

}
