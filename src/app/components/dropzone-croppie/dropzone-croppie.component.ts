import { Component, OnInit, Output, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import Croppie from 'croppie';

@Component({
  selector: 'app-dropzone-croppie',
  templateUrl: './dropzone-croppie.component.html',
  styleUrls: ['./dropzone-croppie.component.css']
})
export class DropzoneCroppieComponent implements OnInit {

  @Input('file') fileImage: any;
  @Output('save') saveEmitter = new EventEmitter();
  @ViewChild('uploadInput') uploadInputHtmlElement: ElementRef;
  @ViewChild('croppie') croppieHtmlElement: ElementRef;

  showCroppie = false;
  editMode = false;
  croppie: any;
  size: any = {width: 1024, height: 768};
  tempFile = null;

  constructor() { }

  ngOnInit(): void {}

  dropImage(event){

    const reader  = new FileReader();
    reader.onloadend = (e) => {

      this.tempFile = e.target.result;

      this.startCroppie();
      this.croppie.bind({
        url: e.target.result
      });
      this.editMode = false;
    };
    reader.readAsDataURL(event.addedFiles[0]);
  }

  startCroppie(){

    this.showCroppie = true;

    const boxCroppie = this.croppieHtmlElement.nativeElement;

    const width = boxCroppie.offsetWidth;

    const viewport = {
      width: this.size.width,
      height: this.size.height,
    };

    const boundary = {
      width: this.size.width + 20,
      height: this.size.height + 20,
    };

    if (width < boundary.width) {
      boundary.width = width;
      viewport.width = width - 20;
    }

    const factor = this.size.height / this.size.width;

    viewport.height = factor * viewport.width;
    boundary.height = factor * boundary.width;

    this.croppie = new Croppie(boxCroppie, {
      viewport,
      boundary
    });
  }


  saveImage() {
    this.croppie.result({
      type: 'blob',
      format: 'png',
      quality: 1,
      size: this.size
    }).then((blob) => {
      this.saveEmitter.emit(blob);
      this.showCroppie = false;

      this.close();
      this.tempFile = null;
    });
  }

  close() {
    this.croppie.destroy();
    this.showCroppie = false;
  }

}
