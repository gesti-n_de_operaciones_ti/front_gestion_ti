import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmployeesService} from '../../api/employees.service';
import {Role} from '../../models/employee';
import Swal from 'sweetalert2';
import {Department} from '../../models/departamentos';
import {DepartmentsService} from '../../api/departments.service';

@Component({
  selector: 'app-modal-create-employee',
  templateUrl: './modal-create-employee.component.html',
  styleUrls: ['./modal-create-employee.component.css']
})
export class ModalCreateEmployeeComponent implements OnInit {

  form: FormGroup;
  roles: Role[];
  departamentos: Department[];
  close: any;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeesService,
    private departmentsService: DepartmentsService,
    public bsModalRef: BsModalRef
    ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      username: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      default_password: [null, [Validators.required, Validators.minLength(8)]],
      idRol: [null, [Validators.required]],
      address: [null, [Validators.required]],
      idDepartamento: [null, [Validators.required]],
      phone: [null],
    });

    this.employeesService.roles().subscribe(res => {
      this.roles = res;
    });

    this.departmentsService.index().subscribe(res => {
      this.departamentos = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  register(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.employeesService.create(dataForm).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Empleado creado exitosamente.',
        text: 'El empleado se ha añadido a los usuarios del sistema.'
      });

      this.bsModalRef.hide();
      this.close();
    });

    console.log(dataForm);
  }

}
