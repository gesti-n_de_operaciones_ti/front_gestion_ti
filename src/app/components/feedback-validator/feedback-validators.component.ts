import {Component, Input} from '@angular/core';

@Component({
  styles : ['.invalid-feedback{display: inline-block;font-size: 80%;color: #dc3545;}'],
  selector : 'app-feedback-validator',
  template :
    `<div *ngIf="submitted && errors" class="invalid-feedback">
      <div *ngIf="errors.required">El campo {{attr}} es requerido.</div>
      <div *ngIf="errors.min">El {{attr}} debe ser mayor que {{errors.min.min}}.</div>
      <div *ngIf="errors.nowhitespace">El campo {{attr}} tiene caracteres de espacio en blanco no válido.</div>
      <div *ngIf="errors.size">El campo {{attr}} debe tener {{errors.size.requiredSize}} caracteres.</div>
      <div *ngIf="errors.numeric">El campo {{attr}} debe ser numérico.</div>
      <div *ngIf="errors.email">El campo {{attr}} debe ser un correo electrónico válido.</div>
      <div *ngIf="errors.asyncproductvalidator">El campo seleccionado {{attr}} es inválido.</div>
      <div *ngIf="errors.asyncepcvalidator">El campo seleccionado {{attr}} is inválido.</div>
      <div *ngIf="errors.asynccveproductovalidator">La clave del producto ya esta en uso.</div>
      <div *ngIf="errors.decimal">El campo seleccionado {{attr}} es inválido.</div>
      <div *ngIf="errors.max">El número máximo admitido para el campo {{attr}} es {{errors.max.max}}.</div>
      <div *ngIf="errors.minLength">El número mínimo de caracteres para el campo {{attr}} es {{errors.min.length}}.</div>
      <div *ngIf="errors.formatoHora">La hora debe ser formato 24 horas (HH:MM:SS).</div>
      <div *ngIf="errors.precio">El {{attr}} debe ser mayor a numérico.</div>
      <!--<pre>{{errors | json}}</pre>-->
    </div>`
})

export class FeedbackValidatorsComponent {

  @Input() submitted;
  @Input() errors;
  @Input() attr = 'Field';
  constructor() {}

}
