import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Edificio} from '../../models/EdificiosLocalizaciones';
import {ActivatedRoute, Router} from '@angular/router';
import {EdificiosLocalizacionesService} from '../../api/edificios-localizaciones.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-edificios',
  templateUrl: './form-edificios.component.html',
  styleUrls: ['./form-edificios.component.css']
})
export class FormEdificiosComponent implements OnInit {

  form: FormGroup;

  idEdificio: string;
  submitted = false;
  action: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private localizacionesService: EdificiosLocalizacionesService
  ) {
    this.form = this.fb.group({
      clave: [null, [Validators.minLength(1), Validators.maxLength(3), Validators.required]],
      descripcion: [null, [Validators.required]],
      descUbicacion: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.idEdificio = this.route.snapshot.params.idEdificio;

    this.route.data.subscribe(data => {
      this.action = data.action;
    });

    if (this.action === 'show'){
      this.localizacionesService.showEdificio(this.idEdificio).subscribe(data => {
        this.form.reset(data);
      });
    }
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    if (this.action === 'create'){
      this.localizacionesService.createEdificio(dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Edificio creado exitosamente.',
          text: 'Ha registrado el edificio con éxito.'
        });

        this.router.navigateByUrl('/admin/edificios');
      });
    }
    else{
      this.localizacionesService.updateEdificio(this.idEdificio, dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Edificio actualizado correctamente.',
          text: 'Los datos del edificio se han actualizado.'
        });

        this.router.navigateByUrl('/admin/edificios');
      });

    }
  }

}
