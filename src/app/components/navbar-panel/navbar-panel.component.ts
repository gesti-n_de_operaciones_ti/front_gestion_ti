import { Component, OnInit } from '@angular/core';
import {Employee} from '../../models/employee';
import {FormBuilder, FormGroup} from '@angular/forms';
import {StorageService} from '../../api/storage.service';
import {Router} from '@angular/router';
import {AuthService} from '../../api/auth.service';

@Component({
  selector: 'app-navbar-panel',
  templateUrl: './navbar-panel.component.html',
  styleUrls: ['./navbar-panel.component.css']
})
export class NavbarPanelComponent implements OnInit {

  form: FormGroup;
  idRol: number;
  labelName: string;

  constructor(
    private fb: FormBuilder,
    private storage: StorageService,
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
    });

    this.labelName = this.user.name.split(' ')[0];
    this.idRol = this.storage.getEmployee().role;
  }

  logout(){
    this.auth.logout().subscribe(res => {
      this.router.navigateByUrl('/login');
    });
  }

  get user(): Employee{
    return this.storage.getEmployee();
  }
}
