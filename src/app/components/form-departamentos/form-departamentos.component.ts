import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {EdificiosLocalizacionesService} from '../../api/edificios-localizaciones.service';
import Swal from 'sweetalert2';
import {DepartamentosComponent} from '../../admin/pages/departamentos/departamentos.component';
import {DepartmentsService} from '../../api/departments.service';
import {EmployeesService} from '../../api/employees.service';
import {Employee} from '../../models/employee';
import {Department} from '../../models/departamentos';

@Component({
  selector: 'app-form-departamentos',
  templateUrl: './form-departamentos.component.html',
  styleUrls: ['./form-departamentos.component.css']
})
export class FormDepartamentosComponent implements OnInit {

  empleados: Employee[];
  form: FormGroup;

  idDepartamento: string;
  submitted = false;
  action: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    private departamentosService: DepartmentsService,
    private empleadosService: EmployeesService
  ) {
    this.form = this.fb.group({
      nbDepartamento: [null, [Validators.required]],
      idEncargado: [null]
    });
  }

  ngOnInit(): void {
    this.idDepartamento = this.route.snapshot.params.idDepartamento;

    this.route.data.subscribe(data => {
      this.action = data.action;
    });

    if (this.action === 'show'){
      this.departamentosService.show(this.idDepartamento).subscribe(data => {
        this.form.reset({
          nbDepartamento: data.nbDepartamento,
          idEncargado: data.encargado?.id
        });
      });
    }

    this.empleadosService.index().subscribe(res => {
      this.empleados = res.data;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    if (this.action === 'create'){
      this.departamentosService.create(dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Departamento creado exitosamente.',
          text: 'Ha registrado el departamento con éxito.'
        });

        this.router.navigateByUrl('/admin/departamentos');
      });
    }
    else{
      this.departamentosService.update(this.idDepartamento, dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Departamento actualizado correctamente.',
          text: 'Los datos del departamento se han actualizado.'
        });

        this.router.navigateByUrl('/admin/departamentos');
      });

    }
  }
}
