import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Employee} from 'src/app/models/employee';
import {Proveedor} from 'src/app/models/proveedores';
import {EmployeesService} from '../../api/employees.service';
import {ProveedoresService} from '../../api/proveedores';
import {ElementoConfiguracionService} from '../../api/elemento-configuracion.service';
import {TipoCI} from '../../models/elemento-configuracion';
import {Department} from '../../models/departamentos';
import {EdificiosLocalizacionesService} from '../../api/edificios-localizaciones.service';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';
import {DepartmentsService} from '../../api/departments.service';
import {Edificio, Localizacion} from '../../models/EdificiosLocalizaciones';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-form-elemento-configuracion',
  templateUrl: './form-elemento-configuracion.component.html',
  styleUrls: ['./form-elemento-configuracion.component.css']
})
export class FormElementoConfiguracionComponent implements OnInit {

  form: FormGroup;
  formDataCI: FormGroup;

  empleados: Employee[];
  proveedores: Proveedor[];

  departamentos: Department[];
  edificios: Edificio[];
  localizaciones: Localizacion[];
  tiposCI: TipoCI[];
  siglasSelectedTipo: string = null;

  submitted = false;
  action: string;

  idElementoCI: string;
  editMode = true;


  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeesService,
    private proveedoresService: ProveedoresService,
    private departmentsService: DepartmentsService,
    private CIService: ElementoConfiguracionService,
    private localizacionesService: EdificiosLocalizacionesService,
    private datePipe: DatePipe,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.formDataCI = this.fb.group({});

    this.form = this.fb.group({
      descripcion: [null, [Validators.required]],
      nbElemento: [null, [Validators.required]],
      idEncargado: [null, [Validators.required]],
      idProveedor: [null],
      fhAdquisicion: [null, [Validators.required]],
      siglasTipoCI: [null, [Validators.required]],
      data_ci: this.formDataCI
    });

    this.getData();
  }

  ngOnInit(): void {
    this.idElementoCI = this.route.snapshot.params.idElemento;

    this.route.data.subscribe(data => {
      this.action = data.action;
    });

    if (this.action === 'show'){

      this.CIService.show(this.idElementoCI).subscribe(data => {
        if(data.fhAdquisicion){
          data.fhAdquisicion = new Date(`${data.fhAdquisicion}T00:00`);
        }
        this.form.reset(data);
        this.changeTipoCI(data.siglasTipoCI, data.data_ci);
      });

      this.disableControls();
    }

  }

  getData(): void{
    this.employeesService.index(null, [1]).subscribe(res => {
      this.empleados = res.data;
    });

    this.proveedoresService.index().subscribe(res => {
      this.proveedores = res;
    });

    this.CIService.tipos().subscribe(res => {
      this.tiposCI = res;
    });
  }

  changeTipoCI(tipoCI: string, dataCI = null){
    this.siglasSelectedTipo = tipoCI;

    switch (this.siglasSelectedTipo){
      case 'HW':
        this.setDataToHardware(dataCI);
        break;
      case 'SW':
        this.setDataToSoftware(dataCI);
        break;
      case 'NET':
        this.setDataToNetwork(dataCI);
        break;
      case 'DB':
        this.setDataToDatabase(dataCI);
        break;
    }

    if (this.action === 'show'){
      this.formDataCI.reset(dataCI);
      this.formDataCI.disable();
    }
  }

  changeEdificio(idEdificio: string){
    this.localizacionesService.index(null, idEdificio).subscribe(res => {
      this.localizaciones = res;
      console.log(res);
    });
  }

  save(): void{
    this.submitted = true;

    if (this.form.invalid || this.formDataCI.invalid) { return; }

    const data = this.form.value;
    data.data_ci = this.formDataCI.value;

    data.fhAdquisicion = this.datePipe.transform(data.fhAdquisicion, 'yyyy-MM-dd');

    if (this.action === 'create'){
      this.CIService.create(data).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Elemento de configuración creado exitosamente.',
          text: 'El elemento de configuración se ha añadido al sistema.'
        });

        this.router.navigateByUrl('/admin/recursos');
      });
    }
    else{
      this.CIService.update(this.idElementoCI, data).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Elemento de configuración actualizado.',
          text: 'Ha actualizado la información del elemento de configuración exitosamente.'
        });

        this.disableControls();
      });
    }

  }

  get formControls(){
    return this.form.controls;
  }

  get formDataCIControls(){
    return this.formDataCI.controls;
  }

  disableControls(){
    this.form.disable();
    this.formDataCI.disable();
    this.editMode = false;
  }

  enableControls(){
    this.form.enable();
    this.formDataCI.enable();
    this.editMode = true;
  }

  private setDataToHardware(dataCI = null){

    this.formDataCI = this.fb.group({
      numSerie: [null, Validators.required],
      modelo: [null, Validators.required],
      ip_address: [null],
      idEdificio: [null],
      idLocalizacion: [null, Validators.required],
    });

    this.localizacionesService.edificios().subscribe(res => {
        this.edificios = res;
    });

    if (this.action === 'show'){
      this.changeEdificio(dataCI.idEdificio);
    }
  }

  private setDataToSoftware(dataCI = null){
    this.formDataCI = this.fb.group({
      version: [null, Validators.required],
      ip_address: [null],
    });
  }

  private setDataToNetwork(dataCI = null){
    this.formDataCI = this.fb.group({
      network_address: [null, Validators.required],
      subnet_mask: [null, Validators.required],
      idDepartamento: [null, Validators.required],
    });

    this.departmentsService.index().subscribe(res => {
      this.departamentos = res;
    });
  }

  private setDataToDatabase(dataCI = null){
    this.formDataCI = this.fb.group({
      dbms_name: [null, Validators.required],
      ip_address: [null, Validators.required],
      version: [null, Validators.required],
    });
  }
}
