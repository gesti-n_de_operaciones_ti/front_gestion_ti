import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {StorageService} from '../../api/storage.service';
import {Employee} from '../../models/employee';
import {AuthService} from '../../api/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'admin-nav-bar',
  templateUrl: './navbar-admin.component.html',
  styleUrls: ['./navbar-admin.component.css']
})
export class NavbarAdminComponent implements OnInit {

  form: FormGroup;
  labelName: string;

  constructor(
    private fb: FormBuilder,
    private storage: StorageService,
    private router: Router,
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
    });

    this.labelName = this.user.name.split(' ')[0];
  }

  logout(){
    this.auth.logout().subscribe(res => {
      this.router.navigateByUrl('/login');
    });
  }

  get user(): Employee{
    return this.storage.getEmployee();
  }

}
