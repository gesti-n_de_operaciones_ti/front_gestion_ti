import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Role} from '../../models/employee';
import {Department} from '../../models/departamentos';
import {EmployeesService} from '../../api/employees.service';
import {DepartmentsService} from '../../api/departments.service';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-form-empleados',
  templateUrl: './form-empleados.component.html',
  styleUrls: ['./form-empleados.component.css']
})
export class FormEmpleadosComponent implements OnInit {

  form: FormGroup;
  roles: Role[];
  departamentos: Department[];
  idEmpleado: string;

  submitted = false;
  action: string;

  constructor(
    private fb: FormBuilder,
    private employeesService: EmployeesService,
    private departmentsService: DepartmentsService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: [null, [Validators.required]],
      name: [null, [Validators.required]],
      last_name: [null, [Validators.required]],
      email: [null, [Validators.required, Validators.email]],
      default_password: [null, [Validators.required, Validators.minLength(8)]],
      idRol: [null, [Validators.required]],
      address: [null, [Validators.required]],
      idDepartamento: [null, [Validators.required]],
      phone: [null],
    });
  }

  ngOnInit(): void {
    this.idEmpleado = this.route.snapshot.params.idEmpleado;

    this.route.data.subscribe(data => {
      this.action = data.action;
    });

    if (this.action === 'show'){
      this.form.removeControl('default_password');

      this.employeesService.show(this.idEmpleado).subscribe(data => {
        this.form.reset(data);
      });
    }

    this.getData();
  }

  getData(){
    this.employeesService.roles().subscribe(res => {
      this.roles = res;
    });

    this.departmentsService.index().subscribe(res => {
      this.departamentos = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;

    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    if (this.action === 'create'){
      this.employeesService.create(dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Empleado creado exitosamente.',
          text: 'El empleado se ha añadido a los usuarios del sistema.'
        });
      });

      this.router.navigateByUrl('/admin/empleados');
    }
    else{
      this.employeesService.update(this.idEmpleado, dataForm).subscribe(res => {
        Swal.fire({
          icon: 'success',
          title: 'Empleado actualizado exitosamente.',
          text: 'Los datos del empleado se han actualizado.'
        });
      });

      this.router.navigateByUrl('/admin/empleados');
    }

  }

}
