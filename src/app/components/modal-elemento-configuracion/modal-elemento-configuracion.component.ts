import { Component, OnInit } from '@angular/core';
import {BsModalRef} from 'ngx-bootstrap/modal';
import {EmployeesService} from '../../api/employees.service';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {ElementoConfiguracionService} from '../../api/elemento-configuracion.service';

@Component({
  selector: 'app-modal-elemento-configuracion',
  templateUrl: './modal-elemento-configuracion.component.html',
  styleUrls: ['./modal-elemento-configuracion.component.css']
})
export class ModalElementoConfiguracionComponent implements OnInit {

  ciElement: ElementoConfiguracion;
  idElementoConf: string;
  close: any;

  constructor(
    public bsModalRef: BsModalRef,
    private elementosConfigService: ElementoConfiguracionService,
  ) { }

  ngOnInit(): void {
    this.elementosConfigService.show(this.idElementoConf).subscribe(data => {
      this.ciElement = data;
    });
  }

}
