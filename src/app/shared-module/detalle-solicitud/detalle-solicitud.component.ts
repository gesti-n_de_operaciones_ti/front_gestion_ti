import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {EvaluacionIncidencia, HistorialSolicitud, Solicitud} from '../../models/solicitud';
import {ElementoConfiguracion} from '../../models/elemento-configuracion';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {SolicitudesService} from '../../api/solicitudes.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detalle-solicitud',
  templateUrl: './detalle-solicitud.component.html',
  styleUrls: ['./detalle-solicitud.component.css']
})
export class DetalleSolicitudComponent implements OnInit {

  bsModalRef: BsModalRef;

  idSolicitud: string;
  solicitud: Solicitud;
  historialSolicitud: HistorialSolicitud[] = [];
  evaluacionIncidencia: EvaluacionIncidencia;

  constructor(
    private solicitudesService: SolicitudesService,
    private modalService: BsModalService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.idSolicitud = this.route.snapshot.params.idIncidencia;

    this.solicitudesService.show(this.idSolicitud).subscribe(res => {
      this.solicitud = res;
    });

    this.solicitudesService.historial(this.idSolicitud).subscribe(res => {
      this.historialSolicitud = res;
    });

    this.solicitudesService.evaluacion(this.idSolicitud).subscribe(res => {
      this.evaluacionIncidencia = res;
    });
  }

  openElementoModal(elemento: ElementoConfiguracion){
    const initialState = {
      idElementoConf: elemento.idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

}
