import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './not-found/not-found.component';
import { RouterModule } from '@angular/router';
import {HeaderComponent} from '../components/header/header.component';
import {ModalModule} from 'ngx-bootstrap/modal';
import {NgxMaskModule} from 'ngx-mask';
import {FeedbackValidatorsComponent} from '../components/feedback-validator/feedback-validators.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import {DropzoneCroppieComponent} from '../components/dropzone-croppie/dropzone-croppie.component';
import {NavbarPanelComponent} from '../components/navbar-panel/navbar-panel.component';
import {BsDatepickerModule, BsLocaleService} from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import {esLocale} from 'ngx-bootstrap/locale';
import {SolicitudesEmpleadosComponent} from '../pages/solicitudes-empleados/solicitudes-empleados.component';
import {ModalCatalogoServiciosComponent} from '../components/modal-catalogo-servicios/modal-catalogo-servicios.component';
import {TableCatalogoServiciosComponent} from './table-catalogo-servicios/table-catalogo-servicios.component';
import {TooltipModule} from 'ngx-bootstrap/tooltip';
import { CambiosSolicitudesComponent } from './cambios-solicitudes/cambios-solicitudes.component';
import { DetalleSolicitudComponent } from './detalle-solicitud/detalle-solicitud.component';
import {DformQuestionComponent} from '../modules/dforms/dform-question/dform-question.component';
import {DformGroupComponent} from '../modules/dforms/dform-group/dform-group.component';


@NgModule({
  declarations: [
    NotFoundComponent,
    HeaderComponent,
    FeedbackValidatorsComponent,
    DropzoneCroppieComponent,
    NavbarPanelComponent,
    SolicitudesEmpleadosComponent,
    ModalCatalogoServiciosComponent,
    TableCatalogoServiciosComponent,
    CambiosSolicitudesComponent,
    DetalleSolicitudComponent,
    DformQuestionComponent,
    DformGroupComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgxMaskModule.forRoot(),
    TooltipModule.forRoot(),
    NgxDropzoneModule,
  ],
  exports: [
    ReactiveFormsModule,
    NotFoundComponent,
    HeaderComponent,
    FeedbackValidatorsComponent,
    NgxMaskModule,
    SolicitudesEmpleadosComponent,
    BsDatepickerModule,
    NgxDropzoneModule,
    DropzoneCroppieComponent,
    NavbarPanelComponent,
    TableCatalogoServiciosComponent,
    ModalCatalogoServiciosComponent,
    TooltipModule,
    DformQuestionComponent,
    DformGroupComponent
  ]
})
export class SharedModule {
  constructor(private bsLocaleService: BsLocaleService) {
    defineLocale('es', esLocale);
    this.bsLocaleService.use('es');
  }
}
