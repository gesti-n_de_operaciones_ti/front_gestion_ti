import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Servicio} from '../../models/servicio';
import {CatalogoServiciosService} from '../../api/catalogo-servicios.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-table-catalogo-servicios',
  templateUrl: './table-catalogo-servicios.component.html',
  styleUrls: ['./table-catalogo-servicios.component.css']
})
export class TableCatalogoServiciosComponent implements OnInit {

  @Output() eventDelete = new EventEmitter<any>();

  @Input() interactive = false;
  @Input() catalogoServicios: Servicio[] = [];

  constructor(
    private catalogoService: CatalogoServiciosService
  ) { }

  ngOnInit(): void {

  }

  eliminarServicio(servicio: Servicio){

    Swal.fire({
      title: `¿Seguro que deseas eliminar el registro del servicio de "${servicio.nbServicio}"?`,
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.catalogoService.delete(servicio.idProblema).subscribe( res => {
          Swal.fire(
            'Servicio del catalogo eliminado exitosamente.',
            'El registro del servicio se ha eliminado de la base de datos correctamente.',
            'success'
          );

          this.eventDelete.emit(servicio);
        });
      }
    });
  }

}
