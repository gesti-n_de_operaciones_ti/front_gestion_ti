import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Prioridad} from '../../models/solicitud';
import {SolicitudesService} from '../../api/solicitudes.service';
import {CatalogoServiciosService} from '../../api/catalogo-servicios.service';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';

@Component({
  selector: 'app-form-catalogo-servicios',
  templateUrl: './form-catalogo-servicios.component.html',
  styleUrls: ['./form-catalogo-servicios.component.css']
})
export class FormCatalogoServiciosComponent implements OnInit {

  form: FormGroup;
  submitted = false;

  prioridades: Prioridad[];

  constructor(
    private fb: FormBuilder,
    private solicitudesService: SolicitudesService,
    private catalogoService: CatalogoServiciosService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      nbServicio: [null, [Validators.required]],
      descripcion: [null, [Validators.required]],
      solucion: [null, [Validators.required]],
      idPrioridad: [null, [Validators.required]],
      tiempoEstimado: [null, [Validators.required, Validators.min(0)]],
    });

    this.solicitudesService.prioridades().subscribe(res => {
      this.prioridades = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;
    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.catalogoService.create(dataForm).subscribe(res => {

      Swal.fire({
        icon: 'success',
        title: 'El servicio se registró correctamente.',
        text: 'Ha agregado un elemento al catalogo de servicios.'
      });

      this.router.navigateByUrl('/tecnico/catalogo-servicios');
    });

  }

}
