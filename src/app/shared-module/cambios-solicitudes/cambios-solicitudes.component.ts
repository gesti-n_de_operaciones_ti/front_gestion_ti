import { Component, OnInit } from '@angular/core';
import {Department} from '../../models/departamentos';
import {DepartmentsService} from '../../api/departments.service';
import {Router} from '@angular/router';
import {CambiosSolicitudesService} from '../../api/cambios-solicitudes.service';
import {SolicitudCambio} from '../../models/solicitud-cambio';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ModalElementoConfiguracionComponent} from '../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';

@Component({
  selector: 'app-cambios-solicitudes',
  templateUrl: './cambios-solicitudes.component.html',
  styleUrls: ['./cambios-solicitudes.component.css']
})
export class CambiosSolicitudesComponent implements OnInit {

  bsModalRef: BsModalRef;
  solicitudesCambio: SolicitudCambio[];

  constructor(
    private cambiosService: CambiosSolicitudesService,
    private modalService: BsModalService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.cambiosService.index().subscribe(res => {
      this.solicitudesCambio = res;
    });
  }

  openElementoModal(idElementoConf: string){
    const initialState = {
      idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

  procesarCambio(solicitud: SolicitudCambio){
    this.router.navigateByUrl(`/admin/cambios/${solicitud.idSolicitudCambio}/evaluar`);
  }

}
