import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminPanelComponent } from './admin-panel.component';
import {EmpleadosComponent} from './pages/empleados/empleados.component';
import {ElementosConfiguracionComponent} from '../elementos-configuracion/elementos-configuracion.component';
import {FormElementoConfiguracionComponent} from '../components/form-elemento-configuracion/form-elemento-configuracion.component';
import {FormEmpleadosComponent} from '../components/form-empleados/form-empleados.component';
import {LocalizacionesComponent} from './pages/localizaciones/localizaciones.component';
import {EdificiosLocalizacionesComponent} from './pages/edificios-localizaciones/edificios-localizaciones.component';
import {EdificiosComponent} from './pages/edificios/edificios.component';
import {DepartamentosComponent} from './pages/departamentos/departamentos.component';
import {FormLocalizacionesComponent} from '../components/form-localizaciones/form-localizaciones.component';
import {FormEdificiosComponent} from '../components/form-edificios/form-edificios.component';
import {FormDepartamentosComponent} from '../components/form-departamentos/form-departamentos.component';
import {SolicitudesEmpleadosComponent} from '../pages/solicitudes-empleados/solicitudes-empleados.component';
import {FormAsignarSolicitudComponent} from './components/form-asignar-solicitud/form-asignar-solicitud.component';
import {CambiosSolicitudesComponent} from '../shared-module/cambios-solicitudes/cambios-solicitudes.component';
import {FormEvaluarCambioSolicitudComponent} from './components/form-evaluar-cambio-solicitud/form-evaluar-cambio-solicitud.component';
import {DetalleSolicitudComponent} from '../shared-module/detalle-solicitud/detalle-solicitud.component';


const routes: Routes = [
  {path: '', redirectTo: 'incidencias', pathMatch: 'full'},
  {
    path: '',
    component : AdminPanelComponent,
    children : [
      {
        path: 'empleados',
        children: [
          {
            path: 'create',
            component : FormEmpleadosComponent,
            data: {action: 'create'}
          },
          {
            path: ':idEmpleado',
            component : FormEmpleadosComponent,
            data: {action: 'show'}
          },
          { path: '', component : EmpleadosComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'incidencias',
        children: [
          { path: '', component : SolicitudesEmpleadosComponent, pathMatch: 'full'},
          {
            path: ':idIncidencia',
            children: [
              {
                path: 'asignar',
                component: FormAsignarSolicitudComponent
              },
              {
                path: 'historial',
                component: DetalleSolicitudComponent
              }
            ]
          },
        ]
      },
      {
        path: 'recursos',
        children: [
          {
            path: 'create',
            component : FormElementoConfiguracionComponent,
            data: {action: 'create'}
          },
          {
            path: ':idElemento',
            component : FormElementoConfiguracionComponent,
            data: {action: 'show'}
          },
          { path: '', component : ElementosConfiguracionComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'localizaciones',
        component: EdificiosLocalizacionesComponent,
        children: [
          {
            path: 'create',
            component : FormLocalizacionesComponent,
            data: {action: 'create'}
          },
          {
            path: ':idLocalizacion',
            component : FormLocalizacionesComponent,
            data: {action: 'show'}
          },
          {path: '', component: LocalizacionesComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'edificios',
        component : EdificiosLocalizacionesComponent,
        children: [
          {
            path: 'create',
            component : FormEdificiosComponent,
            data: {action: 'create'}
          },
          {
            path: ':idEdificio',
            component : FormEdificiosComponent,
            data: {action: 'show'}
          },
          {path: '', component: EdificiosComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'departamentos',
        children: [
          {
            path: 'create',
            component : FormDepartamentosComponent,
            data: {action: 'create'}
          },
          {
            path: ':idDepartamento',
            component : FormDepartamentosComponent,
            data: {action: 'show'}
          },
          {path: '', component: DepartamentosComponent, pathMatch: 'full'}
        ]
      },
      {
        path: 'cambios',
        children: [
          {
            path: ':idSolicitud',
            children: [
              {
                path: 'evaluar',
                component: FormEvaluarCambioSolicitudComponent
              }
            ]
          },
          {path: '', component: CambiosSolicitudesComponent, pathMatch: 'full'}
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
