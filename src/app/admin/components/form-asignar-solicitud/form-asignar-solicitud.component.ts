import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EmployeesService} from '../../../api/employees.service';
import {SolicitudesService} from '../../../api/solicitudes.service';
import {Employee} from '../../../models/employee';
import {Prioridad, Solicitud} from '../../../models/solicitud';
import {ActivatedRoute, Route, Router} from '@angular/router';
import {ElementoConfiguracion} from '../../../models/elemento-configuracion';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {ModalElementoConfiguracionComponent} from '../../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-asignar-solicitud',
  templateUrl: './form-asignar-solicitud.component.html',
  styleUrls: ['./form-asignar-solicitud.component.css']
})
export class FormAsignarSolicitudComponent implements OnInit {

  form: FormGroup;
  bsModalRef: BsModalRef;
  submitted = false;
  idSolicitud: string;

  tecnicos: Employee[];
  solicitud: Solicitud;
  prioridades: Prioridad[];

  constructor(
    private fb: FormBuilder,
    private empleadosService: EmployeesService,
    private solicitudesService: SolicitudesService,
    private modalService: BsModalService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    this.idSolicitud = this.route.snapshot.params.idIncidencia;

    this.form = this.fb.group({
      idPrioridad: [null, [Validators.required]],
      idTecnico: [null, [Validators.required]]
    });
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.empleadosService.index(null, [2]).subscribe(res => {
      this.tecnicos = res.data;
    });

    this.solicitudesService.prioridades().subscribe(res => {
      this.prioridades = res;
    });

    this.solicitudesService.show(this.idSolicitud).subscribe(res => {
      this.solicitud = res;
    });
  }

  get formControls(){
    return this.form.controls;
  }

  save(){
    this.submitted = true;
    if (this.form.invalid) { return; }

    const dataForm = this.form.value;

    this.solicitudesService.asignar(this.idSolicitud, dataForm).subscribe(res => {
      Swal.fire({
        icon: 'success',
        title: 'Incidencia asignada exitosamente.',
        text: 'Ha asignado la incidencia a un técnico para su diagnostico.'
      });

      this.router.navigateByUrl('admin');
    });

  }

  openElementoModal(elemento: ElementoConfiguracion){
    const initialState = {
      idElementoConf: elemento.idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

}
