import { Component, OnInit } from '@angular/core';
import {SolicitudesService} from '../../../api/solicitudes.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Solicitud} from '../../../models/solicitud';
import {ElementoConfiguracion} from '../../../models/elemento-configuracion';
import {ModalElementoConfiguracionComponent} from '../../../components/modal-elemento-configuracion/modal-elemento-configuracion.component';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {SolicitudCambio} from '../../../models/solicitud-cambio';
import {CambiosSolicitudesService} from '../../../api/cambios-solicitudes.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-evaluar-cambio-solicitud',
  templateUrl: './form-evaluar-cambio-solicitud.component.html',
  styleUrls: ['./form-evaluar-cambio-solicitud.component.css']
})
export class FormEvaluarCambioSolicitudComponent implements OnInit {

  idSolicitud: string;
  incidencia: Solicitud;
  solicitudCambio: SolicitudCambio;

  bsModalRef: BsModalRef;

  constructor(
    private solicitudesService: SolicitudesService,
    private cambiosService: CambiosSolicitudesService,
    private route: ActivatedRoute,
    private router: Router,
    private modalService: BsModalService,
  ) { }

  ngOnInit(): void {
    this.idSolicitud = this.route.snapshot.params.idSolicitud;

    this.cambiosService.show(this.idSolicitud).subscribe(res => {
      this.solicitudCambio = res;

      this.getDataSolicitud();
    });
  }

  getDataSolicitud(){
    this.solicitudesService.show(this.solicitudCambio.idIncidencia).subscribe(res => {
      this.incidencia = res;
    });
  }

  openElementoModal(elemento: ElementoConfiguracion){
    const initialState = {
      idElementoConf: elemento.idElementoConf
    };

    this.bsModalRef = this.modalService.show(
      ModalElementoConfiguracionComponent, {initialState, class: 'modal-extra-lg'});
    this.bsModalRef.content.close = () => {

    };
  }

  approveOrDeny(action: string){
    if (action === 'approve'){
      this.cambiosService.aprobar(this.solicitudCambio.idSolicitudCambio, { }).subscribe(res =>{
        Swal.fire({
          icon: 'success',
          title: 'Cambio aprobado.',
          text: 'Ha aprobado el cambio del elemento de configuración. Debe modificar el registro manualmente.'
        });

        this.router.navigateByUrl('/admin/cambios');
      });
    }

    if (action === 'deny'){
      this.cambiosService.rechazar(this.solicitudCambio.idSolicitudCambio, { }).subscribe(res =>{
        Swal.fire({
          icon: 'success',
          title: 'Cambio Rechazado.',
          text: 'Ha rechazado el cambio del elemento de configuración. Se ha cancelado la incidencia.'
        });

        this.router.navigateByUrl('/admin/cambios');
      });
    }
  }

}
