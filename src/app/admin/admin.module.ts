import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared-module/shared-module.module';
import { AdminPanelComponent } from './admin-panel.component';
import {NavbarAdminComponent} from '../components/navbar-admin/navbar-admin.component';
import { EmpleadosComponent } from './pages/empleados/empleados.component';
import {ElementosConfiguracionComponent} from '../elementos-configuracion/elementos-configuracion.component';
import { FormElementoConfiguracionComponent } from '../components/form-elemento-configuracion/form-elemento-configuracion.component';
import {FormEmpleadosComponent} from '../components/form-empleados/form-empleados.component';
import { LocalizacionesComponent } from './pages/localizaciones/localizaciones.component';
import { EdificiosLocalizacionesComponent } from './pages/edificios-localizaciones/edificios-localizaciones.component';
import { EdificiosComponent } from './pages/edificios/edificios.component';
import { DepartamentosComponent } from './pages/departamentos/departamentos.component';
import {FormLocalizacionesComponent} from '../components/form-localizaciones/form-localizaciones.component';
import {FormEdificiosComponent} from '../components/form-edificios/form-edificios.component';
import {FormDepartamentosComponent} from '../components/form-departamentos/form-departamentos.component';
import { FormAsignarSolicitudComponent } from './components/form-asignar-solicitud/form-asignar-solicitud.component';
import { FormEvaluarCambioSolicitudComponent } from './components/form-evaluar-cambio-solicitud/form-evaluar-cambio-solicitud.component';

@NgModule({
  declarations: [
    AdminPanelComponent,
    NavbarAdminComponent,
    EmpleadosComponent,
    ElementosConfiguracionComponent,
    FormElementoConfiguracionComponent,
    FormEmpleadosComponent,
    LocalizacionesComponent,
    EdificiosLocalizacionesComponent,
    EdificiosComponent,
    DepartamentosComponent,
    FormLocalizacionesComponent,
    FormEdificiosComponent,
    FormDepartamentosComponent,
    FormAsignarSolicitudComponent,
    FormEvaluarCambioSolicitudComponent
  ],
  exports: [  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    SharedModule
  ]
})
export class AdminModule { }
