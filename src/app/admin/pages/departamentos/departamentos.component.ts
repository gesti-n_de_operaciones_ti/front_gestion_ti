import { Component, OnInit } from '@angular/core';
import {Edificio} from '../../../models/EdificiosLocalizaciones';
import {EdificiosLocalizacionesService} from '../../../api/edificios-localizaciones.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';
import {Department} from '../../../models/departamentos';
import {DepartmentsService} from '../../../api/departments.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.css']
})
export class DepartamentosComponent implements OnInit {

  departamentos: Department[];

  constructor(
    private departamentosService: DepartmentsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.departamentosService.index({orderBy: 'idDepartamento'}).subscribe(res => {
      this.departamentos = res;
    });
  }

  editDepartamento(departamento: Department){
    this.router.navigateByUrl(`admin/departamentos/${departamento.idDepartamento}`);
  }

  deleteDepartamento(departamento: Department){

    Swal.fire({
      title: `¿Seguro que deseas eliminar el registro del departamento ${departamento.nbDepartamento}?`,
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.departamentosService.delete(departamento.idDepartamento).subscribe(res =>{
          Swal.fire(
            'Departamento eliminado exitosamente',
            'El registro del departamento se ha eliminado de la base de datos correctamente.',
            'success'
          );

          this.getData();
        });
      }
    });
  }
}
