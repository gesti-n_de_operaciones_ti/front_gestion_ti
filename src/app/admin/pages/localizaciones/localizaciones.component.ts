import { Component, OnInit } from '@angular/core';
import {EdificiosLocalizacionesService} from '../../../api/edificios-localizaciones.service';
import {Edificio, Localizacion} from '../../../models/EdificiosLocalizaciones';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-localizaciones',
  templateUrl: './localizaciones.component.html',
  styleUrls: ['./localizaciones.component.css']
})
export class LocalizacionesComponent implements OnInit {

  localizaciones: Localizacion[];

  constructor(
    private localizacionesService: EdificiosLocalizacionesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.localizacionesService.index({orderBy: 'idLocalizacion'}).subscribe(res => {
      this.localizaciones = res;
    });
  }

  editLocalizacion(localizacion: Localizacion){
    this.router.navigateByUrl(`admin/localizaciones/${localizacion.idLocalizacion}`);
  }

  deleteLocalizacion(localizacion: Localizacion){

    Swal.fire({
      title: '¿Seguro que deseas eliminar el registro de la localización?',
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.localizacionesService.delete(localizacion.idLocalizacion).subscribe(res =>{
          Swal.fire(
            'Localización eliminada exitosamente',
            'El registro del localización se ha eliminado de la base de datos correctamente.',
            'success'
          );

          this.getData();
        });
      }
    });
  }

}
