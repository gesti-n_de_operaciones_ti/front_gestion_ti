import { Component, OnInit } from '@angular/core';
import {Employee} from '../../../models/employee';
import {EmployeesService} from '../../../api/employees.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {ModalCreateEmployeeComponent} from '../../../components/modal-create-employee/modal-create-employee.component';
import {ElementoConfiguracion} from '../../../models/elemento-configuracion';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {

  employees: Employee[];

  constructor(
    private employeesService: EmployeesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void{
    this.employeesService.index().subscribe(res => {
      this.employees = res.data;
    });
  }

  editEmpleado(empleado: Employee){
    this.router.navigateByUrl(`admin/empleados/${empleado.id}`);
  }

  deleteEmpleado(empleado: Employee){

    Swal.fire({
      title: '¿Seguro que deseas eliminar el registro del empleado?',
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.employeesService.delete(empleado.id).subscribe(res => {
          Swal.fire(
            'Empleado eliminado exitosamente',
            'El registro del empleado se ha eliminado de la base de datos correctamente.',
            'success'
          );
          this.getEmployees();
        });
      }
    });

  }
}
