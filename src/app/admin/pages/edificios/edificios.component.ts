import { Component, OnInit } from '@angular/core';
import {Edificio, Localizacion} from '../../../models/EdificiosLocalizaciones';
import {EdificiosLocalizacionesService} from '../../../api/edificios-localizaciones.service';
import {Router} from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edificios',
  templateUrl: './edificios.component.html',
  styleUrls: ['./edificios.component.css']
})
export class EdificiosComponent implements OnInit {

  edificios: Edificio[];

  constructor(
    private localizacionesService: EdificiosLocalizacionesService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.localizacionesService.edificios().subscribe(res => {
      this.edificios = res;
    });
  }
  editEdificios(edificio: Edificio){
    this.router.navigateByUrl(`admin/edificios/${edificio.idEdificio}`);
  }

  deleteEdificio(edificio: Edificio){

    Swal.fire({
      title: `¿Seguro que deseas eliminar el registro del edificio ${edificio.clave}?`,
      text: 'Esta acción es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Confirmar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {

        this.localizacionesService.deleteEdificio(edificio.idEdificio).subscribe(res =>{
          Swal.fire(
            'Edificio eliminado exitosamente',
            'El registro del localización se ha eliminado de la base de datos correctamente.',
            'success'
          );

          this.getData();
        });
      }
    });
  }
}
