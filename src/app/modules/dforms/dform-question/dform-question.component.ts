import {Component, Input} from '@angular/core';
import {QuestionBase} from '../question-base';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dform-question',
  templateUrl: './dform-question.component.html'
})
export class DformQuestionComponent {

  @Input() question: QuestionBase<string>;
  @Input() form: FormGroup;
  @Input() submitted: boolean;
  get isValid() { return this.form.controls[this.question.key].valid; }
  get formControl() { return this.form.controls[this.question.key]; }

}
