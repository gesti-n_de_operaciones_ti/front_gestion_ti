import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';
import {QuestionControlService} from '../question-control-service';
import {FormGroup} from '@angular/forms';
import {QuestionBase} from '../question-base';

@Component({
  selector: 'app-dform-group',
  templateUrl: './dform-group.component.html',
  providers: [ QuestionControlService ]
})
export class DformGroupComponent implements  OnChanges {


  @Input() form: FormGroup;
  @Input() questions: QuestionBase<string>[] = [];
  @Input() answers: any = null;
  @Input() readonly = false;
  @Input() submitted = false;

  @Output('save') saveEmitter = new EventEmitter<any>();
  @Output('formCreated') formEmitter = new EventEmitter<FormGroup>();


  constructor(private qcs: QuestionControlService) {  }

  ngOnChanges() {

    if (!this.form){
      this.form = this.qcs.toFormGroup(this.questions);
    }

    this.formEmitter.emit(this.form);

    if (this.answers != null) {
      this.setAnswers(this.answers);
    }
    if (this.readonly) {
      this.form.disable();
    }
  }

  setAnswers(answers) {
    this.form.reset(answers);
  }

  onSubmit() {
    this.saveEmitter.emit(this.form.value);
  }

}
