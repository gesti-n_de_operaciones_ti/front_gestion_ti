import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResourceBase} from './resource';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesService extends ResourceBase{

  constructor(public http: HttpClient)
  {
    super(http, 'solicitudes');
  }

  public prioridades(): Observable<any>{
    return this.http.get(`${this.getURL()}/prioridades`);
  }

  public historial(id: number | string): Observable<any>{
    return this.http.get(`${this.getURL()}/${id}/historial`);
  }

  public evaluacion(id: number | string): Observable<any> {
    return this.http.get(`${this.getURL()}/${id}/evaluacion`);
  }

  public quizEvaluacion(): Observable<any>{
    return this.http.get(`${this.getURL()}/quiz-evaluacion`);
  }

  public asignar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/asignar`, body);
  }

  public diagnosticar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/diagnosticar`, body);
  }

  public procesar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/procesar`, body);
  }

  public evaluar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/evaluar`, body);
  }

}
