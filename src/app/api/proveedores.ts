import { Injectable } from '@angular/core';
import {ResourceBase} from './resource';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class ProveedoresService{

  constructor(
    public http: HttpClient
  ) {
  }

  public index(): Observable<any> {
    return this.http.get(`proveedores`);
  }
}
