import { Injectable, EventEmitter, Output} from '@angular/core';
import {Observable,  throwError} from 'rxjs/index';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor() { }

  @Output() errorEvent: EventEmitter<any> = new EventEmitter(true);

  public handleError(operation = 'operation', result?: any) {
    return (error: HttpErrorResponse): Observable<any> => {

      console.log(error);
      let msg = '';
      if (error.status === 401) {
        msg = 'Usuario y/o contraseña incorrecta, intente de nuevo';
      }else if (error.status === 404) {
        msg = 'La Ruta no se encuentra disponible.';
      }else if (error.status === 403) {
        // msg = error.error.error;
        msg = 'No puede acceder al recurso solicitado.';
      }else if (error.status === 422) {
        // msg = error.error.error;
        msg = error.error;
      }else{
        msg = 'Algo salió mal; intente de nuevo';
      }
      this.errorEvent.emit({msg, error});
      return throwError(msg);
    };

  }
}
