import {EventEmitter, Injectable, Output} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {isNullOrUndefined} from 'util';

export abstract class ResourceBase {

  protected constructor(public http: HttpClient, private url: string) {
  }

  @Output() errorEvent: EventEmitter<any> = new EventEmitter(true);

  setUrl(url: string){ this.url = url; }

  getURL(): string {
    return this.url;
  }

  public index(params?: any): Observable<any> {
    const options = {params};

    return this.http.get(this.url, options);
  }


  public create(body: any): Observable<any> {
    return this.http.post(this.url, body);
  }

  public delete(id: number | string): Observable<any> {
    return this.http.delete(`${this.url}/${id}`);
  }


  public show(id: number|string): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  public update(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.url}/${id}`, body);
  }

}
