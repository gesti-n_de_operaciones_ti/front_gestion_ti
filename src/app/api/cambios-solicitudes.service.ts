import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ResourceBase} from './resource';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CambiosSolicitudesService extends ResourceBase{

  constructor(
    public http: HttpClient
  ) {
    super(http, 'cambios-solicitudes');
  }

  public aprobar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/approve`, body);
  }

  public rechazar(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.getURL()}/${id}/deny`, body);
  }

}
