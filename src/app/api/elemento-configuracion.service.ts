import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ResourceBase} from './resource';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ElementoConfiguracionService extends ResourceBase{

  constructor(
    public http: HttpClient
  ) {
    super(http, 'elementos-configuracion');
  }

  public tipos(): Observable<any> {
    return this.http.get(`tipos_ci`);
  }
}
