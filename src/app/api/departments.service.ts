import { Injectable } from '@angular/core';
import {ResourceBase} from './resource';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentsService extends ResourceBase{

  constructor(
    public http: HttpClient
  ) {
    super(http, 'departments');
  }

  public localizaciones(): Observable<any> {
    return this.http.get(`localizaciones`);
  }
}
