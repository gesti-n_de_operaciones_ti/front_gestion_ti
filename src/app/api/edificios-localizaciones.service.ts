import { Injectable } from '@angular/core';
import {ResourceBase} from './resource';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class EdificiosLocalizacionesService extends ResourceBase{

  private urlEdificios: string;

  constructor(public http: HttpClient) {
    super(http, 'localizaciones');
    this.urlEdificios = `${this.getURL()}/edificios`;
  }

  public index(params?: any, idEdificio?: string): Observable<any> {

    const options = new HttpParams({fromObject: params})
      .set('idEdificio', isNullOrUndefined(idEdificio) ? '' : idEdificio);

    return this.http.get(this.getURL(), {params: options});
  }

  public edificios(): Observable<any> {
    return this.http.get(`${this.urlEdificios}`);
  }

  public createEdificio(body: any): Observable<any> {
    return this.http.post(this.urlEdificios, body);
  }

  public deleteEdificio(id: number | string): Observable<any> {
    return this.http.delete(`${this.urlEdificios}/${id}`);
  }

  public showEdificio(id: number|string): Observable<any> {
    return this.http.get(`${this.urlEdificios}/${id}`);
  }

  public updateEdificio(id: number | string, body: any): Observable<any> {
    return this.http.put(`${this.urlEdificios}/${id}`, body);
  }

}
