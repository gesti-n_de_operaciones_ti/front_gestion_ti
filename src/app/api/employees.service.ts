import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ResourceBase} from './resource';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService extends ResourceBase{

  constructor(
    public http: HttpClient
  ) {
    super(http, 'employees');
  }

  public index(params?: any, roles?: any[]): Observable<any> {

    const options = new HttpParams({fromObject: params})
      .set('roles', isNullOrUndefined(roles) ? '' : roles.join(','));

    return this.http.get(this.getURL(), {params: options});
  }

  public roles(): Observable<any> {
    return this.http.get(`${this.getURL()}/roles`);
  }

}
