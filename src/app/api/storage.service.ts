import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import {Employee} from '../models/employee';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  helper: JwtHelperService;
  constructor() {
    this.helper = new JwtHelperService();
   }

  getToken(): string{
    const json = localStorage.getItem(environment.TOKEN);
    let token: string = null;

    if (json !== null) {
      token = (JSON.parse(json));
    }

    return token;
  }

  public getEmployee(): Employee {

    const json = this.decodeToken();
    let user: any = null;

    if (json !== null) {
      user = json.user;
    }

    return user;
  }

  storeToken(token){
    localStorage.setItem(environment.TOKEN, token);
  }

  isLoggedIn(){
    return this.getToken() !== null && !this.helper.isTokenExpired(this.getToken());
  }

  public logout(): void {
    localStorage.removeItem(environment.TOKEN);
  }

  decodeToken(){
    return this.helper.decodeToken(this.getToken());
  }
}
