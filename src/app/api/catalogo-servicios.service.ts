import { Injectable } from '@angular/core';
import {ResourceBase} from './resource';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CatalogoServiciosService extends ResourceBase{

  constructor(
    public http: HttpClient
  ) {
    super(http, 'catalogo-servicios');
  }

}
