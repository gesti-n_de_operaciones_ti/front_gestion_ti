import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {catchError, map, retry, tap} from 'rxjs/internal/operators';
import { StorageService } from './storage.service';
import {error} from '@angular/compiler/src/util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private storage: StorageService) {
   }

    public login(username: string, password: string): Observable<any> {

      return this.http.post('auth/login', {username, password} )
        .pipe(
          map((res: any) => JSON.stringify(res)),
          tap((res: string) => this.storage.storeToken(res)),
          map((res: any) => JSON.parse(res)),
        );
  }

  public logout(): Observable<any> {

    return this.http.post('auth/logout', {} )
      .pipe(
        tap((res: string) => {
          this.storage.logout();
        },
          (error: any) => {
            this.storage.logout();
          }
        )
      );

  }


}
